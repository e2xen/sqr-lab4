package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


public class UserDAOTests {
    private static JdbcTemplate jdbcMock;
    private static UserDAO userDAO;
    private static final String NICKNAME = "nickname";
    private static final String EMAIL = "email";
    private static final String FULLNAME = "fullname";
    private static final String ABOUT = "about";

    @BeforeEach
    void init() {
        jdbcMock = mock(JdbcTemplate.class);
        userDAO = new UserDAO(jdbcMock);
    }

    private static Stream<Arguments> branches() {
        String prefix = "UPDATE \"users\" SET ";
        return Stream.of(
                // each parameter needs to be tested, since each of them affects the result
                Arguments.of(NICKNAME, EMAIL, null, null, prefix + " email=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, EMAIL, FULLNAME, null, prefix + " email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, EMAIL, null, ABOUT, prefix + " email=? , about=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, EMAIL, FULLNAME, ABOUT, prefix + " email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, null, FULLNAME, null, prefix + " fullname=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, null, FULLNAME, ABOUT, prefix + " fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Arguments.of(NICKNAME, null, null, ABOUT, prefix + " about=?  WHERE nickname=?::CITEXT;")
        );
    }

    @Test
    void mcdcTestChange_whenEveryParameterIsNull() {
        User user = new User(NICKNAME, null, null, null);
        UserDAO.Change(user);
        verify(jdbcMock, never()).update(Mockito.any(), (Object[]) Mockito.any());
    }

    @ParameterizedTest
    @MethodSource("branches")
    void mcdcTestChange(String nickname, String email, String fullName, String about, String expected) {
        User user = new User(nickname, email, fullName, about);
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(expected), (Object[]) Mockito.any());
    }
}
