package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

public class PostDAOTests {
    private static JdbcTemplate mockJdbc;
    private static PostDAO postDao;
    private static Post post;
    private static final Integer POST_ID = 1;
    private static final String AUTHOR = "author";
    private static final String MESSAGE = "message";
    private static final Timestamp CREATED = new Timestamp(System.currentTimeMillis());

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        postDao = new PostDAO(mockJdbc);

        post = new Post();
        post.setId(POST_ID);
        post.setAuthor("");
        post.setMessage("");
        post.setCreated(new Timestamp(0));
    }

    private static Stream<Arguments> params() {
        String prefix = "UPDATE \"posts\" SET ";
        return Stream.of(
                Arguments.of(POST_ID, AUTHOR, null, null, prefix + " author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, AUTHOR, MESSAGE, null, prefix + " author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, AUTHOR, null, CREATED, prefix + " author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, AUTHOR, MESSAGE, CREATED, prefix + " author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, null, MESSAGE, null, prefix + " message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, null, MESSAGE, CREATED, prefix + " message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(POST_ID, null, null, CREATED, prefix + " created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }

    @Test
    void basisPathTestSetPost_whenEveryParameterIsNull() {
        Post p = new Post();
        PostDAO.setPost(POST_ID, p);
        verify(mockJdbc, never()).update(Mockito.any(), (Object[]) Mockito.any());
    }

    @ParameterizedTest
    @MethodSource("params")
    void basisPathTestSetPost(Integer id, String author, String message, Timestamp created, String expected) {
        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(POST_ID))
        ).thenReturn(post);

        Post p = new Post();
        p.setId(id);
        p.setAuthor(author);
        p.setMessage(message);
        p.setCreated(created);

        PostDAO.setPost(POST_ID, p);

        verify(mockJdbc).update(Mockito.eq(expected), (Object[]) Mockito.any());
    }
}
