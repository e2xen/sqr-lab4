package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 We need as few as 3 tests for full branch coverage. Comments show the evaluation of if branches for each test case.
 */
public class ForumDAOTests {
    private static JdbcTemplate mockJdbc;
    private static ForumDAO forumDao;
    private static final String SLUG = "slug";
    private static final String SINCE = "nickname";

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        forumDao = new ForumDAO(mockJdbc);
    }

    private static Stream<Arguments> branches() {
        String prefix = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext";
        return Stream.of(
                // false false false
                Arguments.of(SLUG, null, null, null, prefix + " ORDER BY nickname;"),
                // true->true true true
                Arguments.of(SLUG, 1, SINCE, true, prefix + " AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                // true->false false false
                Arguments.of(SLUG, null, SINCE, null, prefix + " AND  nickname > (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @MethodSource("branches")
    void branchTestUserList(String slug, Number limit, String since, Boolean desc, String expected) {
        ForumDAO.UserList(slug, limit, since, desc);
        verify(mockJdbc).query(Mockito.eq(expected), (Object[]) Mockito.any(), Mockito.any(UserDAO.UserMapper.class));
    }
}
