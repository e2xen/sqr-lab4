package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {
    private static JdbcTemplate mockJdbc;
    private static ThreadDAO threadDAO;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        threadDAO = new ThreadDAO(mockJdbc);
    }

    private static Stream<Arguments> params() {
        String prefix = "SELECT * FROM \"posts\" WHERE thread = ? ";
        return Stream.of(
                Arguments.of(1, 1, 1, true, prefix + " AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Arguments.of(1, null, 1, null, prefix + " AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;")
        );
    }

    @ParameterizedTest
    @MethodSource("params")
    void statementTestTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String expected) {
        ThreadDAO.treeSort(id, limit, since, desc);
        verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }
}
